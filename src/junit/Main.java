package junit;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@Retention(RetentionPolicy.RUNTIME)
@interface MyTest {

}


public class Main {

    final String WORD_TO_SEARCH = "@MyTest";
    final String PROJECT_DIR = "C:\\Users\\Goshoy\\eclipse-workspace\\MyJUnit";
    final String DESTINATION = "C:\\Users\\Goshoy\\eclipse-workspace\\MyJUnit\\test\\myjunit";
    final String BASE_FILE_PATH =
            "C:\\Users\\Goshoy\\eclipse-workspace\\MyJUnit\\test\\myjunit\\FirstTest.java";
    final int FILES_TO_MAKE = 5000;
    final int NUMBER_OF_THREADS = 4;

    List<String> packagePaths = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException, ClassNotFoundException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException, InstantiationException, IOException {

        Main main = new Main();

        FilePathsExtractor fileExtractor =
                new FilePathsExtractor(main.PROJECT_DIR, main.WORD_TO_SEARCH);
        TestGenerator testGenerator =
                new TestGenerator(main.BASE_FILE_PATH, main.DESTINATION, main.FILES_TO_MAKE);
        // testGenerator.generateTestMethods();
        fileExtractor.extractFilePathsOfClassesWithAnnotationsOnMethods();

        Factory factory = new Factory(main.NUMBER_OF_THREADS, fileExtractor.pathsWithAnnotations);
        SingleThreaded singleOrMulti = factory.getInstance("multi");

        long startTime = System.currentTimeMillis();
        if (singleOrMulti != null) {
            singleOrMulti.runAllTestMethods();
        }
        long endTime = System.currentTimeMillis();

        System.out.println(
                "Time for " + main.NUMBER_OF_THREADS + " threads is " + (endTime - startTime));
    }

}
