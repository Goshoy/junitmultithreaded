package junit;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class Factory {

    private Map<String, SingleThreaded> singleOrMulti = new HashMap<>();

    public Factory(int numberOfThreads, BlockingQueue<String> pathsWithAnnotations) {
        singleOrMulti.put("single", new SingleThreaded(pathsWithAnnotations));
        singleOrMulti.put("multi", new Multithreaded(numberOfThreads, pathsWithAnnotations));
    }

    public SingleThreaded getInstance(String type) {
        if (singleOrMulti.containsKey(type)) {
            return singleOrMulti.get(type);
        } else {
            System.out.println("Invalid type!");
        }

        return null;
    }
}
