package junit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TestGenerator {

    final String baseFilePath;
    final String destination;
    final int numberOfFilesToMake;

    public TestGenerator(String baseFilePath, String destination, int numberOfFilesToMake) {
        this.baseFilePath = baseFilePath;
        this.destination = destination;
        this.numberOfFilesToMake = numberOfFilesToMake;
    }

    public void generateTestMethods() throws IOException {
        File rootFile = new File(baseFilePath);
        File destDir = new File(destination);
        destDir.mkdirs();

        List<String> baseFileLines = new ArrayList<>();
        BufferedReader reader = Files.newBufferedReader(Paths.get(baseFilePath));
        String line;

        while ((line = reader.readLine()) != null) {
            baseFileLines.add(line);
        }

        reader.close();

        String rootFileName =
                rootFile.getName().toString().substring(0, rootFile.getName().length() - 5);

        for (int i = 0; i < numberOfFilesToMake; i++) {
            String newFileName = destination + "\\" + rootFileName + i + ".java";
            Path newFile = Files.createFile(Paths.get(newFileName));
            String firstLine = "package " + destDir.getName() + ";";
            BufferedWriter writer = Files.newBufferedWriter(newFile);
            writer.write(firstLine);
            writer.newLine();
            writer.write("public class " + rootFileName + i + "{");
            writer.newLine();
            for (int j = 4; j < baseFileLines.size(); j++) {
                writer.write(baseFileLines.get(j));
                writer.newLine();
            }
            writer.close();
        }
    }

}
