package junit;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    SingleThreaded single;

    public Consumer(BlockingQueue<String> pathsWithAnnotations) {
        single = new SingleThreaded(pathsWithAnnotations);
    }

    @Override
    public void run() {
        try {
            single.runAllTestMethods();
        } catch (SecurityException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
