package junit;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Multithreaded extends SingleThreaded {

    private final int numberOfThreads;

    public Multithreaded(int numberOfThreads, BlockingQueue<String> pathsWithAnnotations) {
        super(pathsWithAnnotations);
        this.numberOfThreads = numberOfThreads;
    }

    @Override
    public void runAllTestMethods() {

        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            executor.execute(new Consumer(super.pathsWithAnnotations));
        }

        executor.shutdown();
        try {
            executor.awaitTermination(40, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
