package junit;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class SingleThreaded {
    BlockingQueue<String> pathsWithAnnotations;

    public SingleThreaded(BlockingQueue<String> pathsWithAnnotations) {
        this.pathsWithAnnotations = pathsWithAnnotations;
    }

    public void runAllTestMethods() throws SecurityException, InterruptedException {
        String path;
        while ((path = pathsWithAnnotations.poll(50, TimeUnit.MILLISECONDS)) != null) {
            Class class1 = null;

            try {
                class1 = Class.forName(path);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            Method methods[] = class1.getDeclaredMethods();

            for (Method method : methods) {
                Annotation annotations[] = method.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation instanceof MyTest) {
                        Class paramTypes[] = method.getParameterTypes();
                        Object methObj = null;

                        try {
                            methObj = class1.newInstance();
                        } catch (InstantiationException | IllegalAccessException e1) {
                            e1.printStackTrace();
                        }

                        try {
                            method.invoke(methObj, paramTypes);
                        } catch (IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
