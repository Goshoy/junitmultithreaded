package junit;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class JavaFilesParser implements Runnable {

    final String PROJECT_DIR;
    BlockingQueue<String> allPaths;
    BlockingQueue<String> pathsWithAnnotations;

    public JavaFilesParser(String PROJECT_DIR, BlockingQueue<String> allPaths,
            BlockingQueue<String> pathsWithAnnotations) {
        this.PROJECT_DIR = PROJECT_DIR;
        this.allPaths = allPaths;
        this.pathsWithAnnotations = pathsWithAnnotations;
    }

    @Override
    public void run() {

        final int ROOT_LENGTH = PROJECT_DIR.length();
        final int SIZE_OF_JAVA_EXTENSION = 5;

        String currentPath;

        try {
            while ((currentPath = allPaths.poll(50, TimeUnit.MILLISECONDS)) != null) {
                int currentPathLength = currentPath.length();
                StringBuilder sb = new StringBuilder(currentPath
                        .substring(ROOT_LENGTH + 1, currentPathLength - SIZE_OF_JAVA_EXTENSION)
                        .replaceAll("\\\\", "."));
                String[] args = sb.toString().split("\\.");

                // Substring the source folder - src.myjuint, from this code "src." will be deleted
                pathsWithAnnotations.add(sb.toString().substring(args[0].length() + 1));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
