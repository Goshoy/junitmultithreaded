package junit;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FilePathsExtractor {

    final String PROJECT_DIR;
    final String WORD_TO_SEARCH;
    BlockingQueue<String> allPaths = new ArrayBlockingQueue<String>(100000);
    BlockingQueue<String> pathsWithAnnotations = new ArrayBlockingQueue<>(100000);

    public FilePathsExtractor(String projectDir, String wordToSearch) {
        this.PROJECT_DIR = projectDir;
        WORD_TO_SEARCH = wordToSearch;
    }

    private void extractFilePaths(String current) throws InterruptedException {
        File file = new File(current);
        File[] files = file.listFiles();

        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                extractFilePaths(files[i].getPath());
            } else if (isJavaFile(files[i])) {
                allPaths.put(files[i].getPath());
            }
        }
    }

    private boolean isJavaFile(File file) {
        String filePath = file.getPath();
        return filePath.endsWith(".java");
    }

    public void extractFilePathsOfClassesWithAnnotationsOnMethods() throws InterruptedException {

        extractFilePaths(PROJECT_DIR);

        ExecutorService executor = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 4; i++) {
            executor.execute(new JavaFilesParser(PROJECT_DIR, allPaths, pathsWithAnnotations));
        }

        executor.shutdown();
        try {
            executor.awaitTermination(40, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}

